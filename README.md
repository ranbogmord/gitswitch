# Gitswitch
Simple tool to switch between different GIT user name/email

## Installation
[Download the binary](https://gitlab.com/ranbogmord/gitswitch/-/releases) and install it into your `$PATH`

## Configuration
Place a file called `.gitswitch.yaml` in your `$HOME`.
The file is a YAML file containing your profile. It looks something like this:
```yaml
personal:
  name: Test Testsson
  email: test@example.com
work:
  name: Test Testsson
  email: test.t@example.com
```

## Usage
```
Usage: gitswitch <alias>
```
