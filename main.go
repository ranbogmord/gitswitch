package main

import (
	"fmt"
	"github.com/spf13/viper"
	"os"
	"os/exec"
)

func usage() {
	fmt.Printf("Usage: %s <alias>\n", os.Args[0])
}

func main() {
	os.Exit(gitswitcher())
}

func gitswitcher() int {
	homedir, err := os.UserHomeDir()
	if err != nil {
		fmt.Println("unable to read user $HOME")
		return 1
	}

	viper.SetConfigName(".gitswitch.yaml")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(homedir)
	err = viper.ReadInConfig()

	if err != nil {
		fmt.Println("unable to read gitswitch config")
		fmt.Println("tried paths:", homedir)
		return 1
	}

	if len(os.Args) < 2 {
		usage()
	} else {
		alias := os.Args[1]

		aliasExists := viper.Get(alias)
		if aliasExists == nil {
			fmt.Println("invalid alias")
			return 2
		}

		config := viper.GetStringMapString(alias)

		if _, ok := config["name"]; !ok {
			fmt.Println("missing variable 'name' in alias")
			return 2
		}
		if _, ok := config["email"]; !ok {
			fmt.Println("missing variable 'name' in alias")
			return 2
		}

		err = exec.Command("git", "config", "--global", "user.name", config["name"]).Run()
		if err != nil {
			fmt.Println("unable to set user.name")
			return 3
		}
		err = exec.Command("git", "config", "--global", "user.email", config["email"]).Run()
		if err != nil {
			fmt.Println("unable to set user.email")
			return 3
		}

		fmt.Printf("switched to profile '%s'\n", alias)
	}

	return 0
}
