clean:
	if [ -d "bin" ]; then rm -r bin; fi

build_linux_amd64:
	GOOS=linux GOARCH=amd64 go build -o bin/gitswitch-linux-amd64 .

build_linux_arm64:
	GOOS=linux GOARCH=arm64 go build -o bin/gitswitch-linux-arm64 .

build_win_amd64:
	GOOS=windows GOARCH=amd64  go build -o bin/gitswitch-win64.exe .


build: build_linux_arm64 build_linux_amd64 build_win_amd64
